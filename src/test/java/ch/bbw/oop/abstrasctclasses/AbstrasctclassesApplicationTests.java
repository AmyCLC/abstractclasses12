package ch.bbw.oop.abstrasctclasses;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.awt.*;

@SpringBootTest
class AbstrasctclassesApplicationTests {

	@Test
	void contextLoads() {
		Garage garage = new Garage();
		garage.addFahrzeug(new Auto("VW", 120, 5, 4, Color.RED, 4, 10));

		garage.printFahrzeuge();
	}

}
