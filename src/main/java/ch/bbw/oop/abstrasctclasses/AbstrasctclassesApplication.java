package ch.bbw.oop.abstrasctclasses;

import com.sun.tools.javac.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;

@SpringBootApplication
public class AbstrasctclassesApplication {


	public static void main(String[] args){
		Garage garage = new Garage();

		garage.addFahrzeug(new Auto("Nissan", 120, 5, 4, Color.RED, 4, 10));
		garage.addFahrzeug(new Auto("Bro", 120, 5, 4, Color.BLACK, 5, 5));
		garage.addFahrzeug(new Auto("Dono", 120, 5, 4, Color.BLUE, 7, 10));
		garage.addFahrzeug(new Auto("Fiat", 120, 5, 4, Color.RED, 4, 2));
		garage.addFahrzeug(new Auto("Liat", 120, 5, 4, Color.RED,2, 2 ));

		garage.addFahrzeug(new Motorrad("Vespa", 11, 2 , 5, Color.BLACK , "Big Choncka", 1 ));



		garage.printFahrzeuge();



	}
}
