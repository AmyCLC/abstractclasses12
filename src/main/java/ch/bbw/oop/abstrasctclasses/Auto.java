package ch.bbw.oop.abstrasctclasses;

import java.awt.*;

public class Auto extends MotorFahrzeug {
    private int doors;

    public Auto(String brand, int power, int seating, int cylinder, Color color, int doors, int space) {
        super(brand, power, seating, cylinder, color, space);
        this.doors = doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }



    public int getDoors() {
        return doors;
    }

    @Override
    void print() {
        super.print();
        System.out.println("Doors: " + doors);
    }
}
