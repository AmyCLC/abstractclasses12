package ch.bbw.oop.abstrasctclasses;

import java.util.ArrayList;

public class  Garage{


   private ArrayList<MotorFahrzeug> fahrzeuge;
   private int max = 20;
   private int available = max;
   private int used = 0;

    public Garage() {
        this.fahrzeuge  = new ArrayList<>();

    }

    public int used(){
        return used;
    }

    public int getMax() {
        return max;
    }
    public int getAvailable(){
        return available;
    }

    public void setMax(int max) {
        this.max = max;
    }

    boolean addFahrzeug(MotorFahrzeug fahrzeug){

        if(available >= used){
            fahrzeuge.add(fahrzeug);
            available = available - fahrzeug.getSpace();
            used = used + fahrzeug.getSpace();


            return true;
        }else{
            System.out.println("No space");
           return false;

        }


    }

    void printFahrzeuge(){

        fahrzeuge.forEach(MotorFahrzeug::print);


    }

    public ArrayList<MotorFahrzeug> getFahrzeuge(){
        return fahrzeuge;

    }


}
