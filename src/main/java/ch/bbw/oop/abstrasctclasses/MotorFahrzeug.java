package ch.bbw.oop.abstrasctclasses;

import java.awt.*;

public abstract class MotorFahrzeug {

    private String brand;
    private int power;
    private int seating;
    private int cylinder;
    private Color color;
    private int space;


    public MotorFahrzeug(String brand, int power, int seating, int cylinder, Color color, int space) {
        this.brand = brand;
        this.power = power;
        this.seating = seating;
        this.cylinder = cylinder;
        this.color = color;
        this.space = space;


    }


    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getSeating() {
        return seating;
    }

    public void setSeating(int seating) {
        this.seating = seating;
    }

    public int getCylinder() {
        return cylinder;
    }

    public void setCylinder(int cylinder) {
        this.cylinder = cylinder;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }


    void print(){

        System.out.println(brand + " " + power + " " + seating + " " +  cylinder + " " + color + " " + space);



    }
}
