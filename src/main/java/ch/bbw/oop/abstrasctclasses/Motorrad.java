package ch.bbw.oop.abstrasctclasses;

import java.awt.*;

public class Motorrad  extends MotorFahrzeug{

    public Motorrad(String brand, int power, int seating, int cylinder, Color color, String type, int space) {
        super(brand, power, seating, cylinder, color, space);
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }
    private String type;

    public String getType() {
        return type;
    }

    @Override
    void print() {
        super.print();
        System.out.println(type);
    }
}
